const { distribution, liveVideo, o2o, supplier } = require('./index')

module.exports = {
	NODE_ENV: '"test"',
	ENV_CONFIG: '"test"',
  DISTRIBUTION: distribution,
  LIVEVIDEO: liveVideo,
  O2O: o2o,
  SUPPLIER: supplier
}
